#include <OneWire.h>
#include <DallasTemperature.h>

// OneWire DS18S20, DS18B20, DS1822 Temperature Example
//
// http://www.pjrc.com/teensy/td_libs_OneWire.html
//
// The DallasTemperature library can do all this work for you!
// http://milesburton.com/Dallas_Temperature_Control_Library

// Data line goes to digital pin 3
#define ONE_WIRE_BUS 3
// Setup a oneWire instance to communicate with any
// OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);
// arrays to hold device addresses
DeviceAddress device0, device1, device2, device3;

int baud_rate = 9600;

void setup(void) {  
  // start serial port
  Serial.begin(baud_rate);
  // Start up the library
    
  sensors.begin();
  // locate devices on the bus
  Serial.print("Locating devices...");
  Serial.print("Found ");
  Serial.print(sensors.getDeviceCount(), DEC);
  Serial.println(" devices.");

  if (!sensors.getAddress(device0, 0))
  {
    Serial.println("Unable to find address for Device 0");
  }
  if (!sensors.getAddress(device1, 1))
  {
    Serial.println("Unable to find address for Device 1");
  }

  if (!sensors.getAddress(device2, 2))
  {
    Serial.println("Unable to find address for Device 2");
  }

  if (!sensors.getAddress(device3, 3))
  {
    Serial.println("Unable to find address for Device 3");
  }
  
  //sensors.setResolution(12);
  
 // print the addresses of both devices
  Serial.print("Device 0 Address: ");
  printAddress(device0);
  Serial.println();
  Serial.print("Device 1 Address: ");
  printAddress(device1);
  Serial.println();
  Serial.print("Device 2 Address: ");
  printAddress(device2);
  Serial.println();
  Serial.print("Device 3 Address: ");
  printAddress(device3);
  Serial.println();
  Serial.println();
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (int i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

// function to print the temperature for a device
void printTemperature(DeviceAddress deviceAddress)
{
  float tempC = sensors.getTempC(deviceAddress);
  Serial.print(tempC);
}

// main function to print information about a device
void printData(DeviceAddress deviceAddress)
{
  printAddress(deviceAddress);
  Serial.print(";");
  printTemperature(deviceAddress);
  Serial.println();
}

void loop(void) {
   // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  //Serial.print("Requesting temperatures...");
  sensors.requestTemperatures();
  //Serial.println("DONE");
  delay(5000);
  // print the device information
  printData(device0);
  printData(device1);
  printData(device2);
  printData(device3);
  delay(55000);
}

