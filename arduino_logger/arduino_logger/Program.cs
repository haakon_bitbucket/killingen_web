﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace arduino_logger
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new LoggerService()
            };
            bool debug = true;
            if (debug == true)
            {
                LoggerService logger = ServicesToRun[0] as LoggerService;
                logger.DoLogging();
            }
            else
            {
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
