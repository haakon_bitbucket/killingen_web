﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DataContext;

namespace arduino_logger
{
    public partial class LoggerService : ServiceBase
    {
        public DataContext.TemperaturDataClassesDataContext db;
        //public string sensor1 = "28B27DDC06000034";
        //public string sensor2 = "286D4DDD060000DF";
        //public string sensor3 = "28902EDD060000B8";
        //public string sensor4 = "28EF7EDD06000091";
        public ReadUSB usb;
        bool logToConsole = false;
        bool logToDatabase = true;

        public LoggerService()
        {
            InitializeComponent();

            logToConsole = Properties.arduino_logger.Default.LogToConsole;
            logToDatabase = Properties.arduino_logger.Default.NoDatabase == false;
        }

        public void DoLogging()
        {
            while (true)
            {
                try
                {
                    using (db = new DataContext.TemperaturDataClassesDataContext())
                    {
                        using (usb = new ReadUSB())
                        {
                            while (true)
                            {
                                string dat = usb.ReadString();
                                AddDatapoint(dat);
                                Thread.Sleep(10000);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (logToConsole)
                    {
                        Console.WriteLine("DoLogging exception handler: {0}", ex.Message);
                        Thread.Sleep(10000);
                    }
                }
            }
        }
        
        public void AddDatapoint(string dataPoint)
        {
            try
            {
                var item = dataPoint.Split(';');
                var sensorNavn = item[0];
                if (sensorNavn.Length != 16)
                    return;

                double y_val;
                var sep = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator;
                if (sep == ",")
                    y_val = double.Parse( item[1].Replace(".", ","));
                else
                    y_val = double.Parse(item[1]);

                if (y_val < -126.0)
                    return;
                
                if (85 == Math.Round(y_val))    //85 is device reset initial value
                    return;

                y_val = Math.Round(y_val, 2);  //Round off to 2 decimals. Precision for device os +- 0,5 deg

                if (logToDatabase)
                {
                    var sensorId = (from s in db.Sensors where s.Navn == sensorNavn select s.ID).First();
                    var temp = new DataContext.Temperatur();
                    temp.SensorID = sensorId;
                    temp.Verdi = y_val;
                    temp.Tid = DateTime.Now;
                    db.Temperaturs.InsertOnSubmit(temp);
                    db.SubmitChanges();
                    if (logToConsole)
                        Console.WriteLine("Sensor {0}; {1} {2}", temp.SensorID, temp.Tid, temp.Verdi);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("AddDataPoint: " + ex.Message);
            }
        }

        protected override void OnStart(string[] args)
        {
        }

        protected override void OnStop()
        {
        }
    }
}
