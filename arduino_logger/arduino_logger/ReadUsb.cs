﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace arduino_logger
{
    public class ReadUSB :IDisposable
    {
        public SerialPort port;
        const int max_tail = 50;
        byte[] inputbuffer = new byte[max_tail];
        int headpos;
        int tailpos;
        bool logToConsole { get; set; }
        string lastGoodSerialPort = "";

        public ReadUSB()
        {
            logToConsole = Properties.arduino_logger.Default.LogToConsole;
            OpenUSBSerial();
        }

        public void Dispose()
        {
            port.Close();
        }

        private void OpenUSBSerial()
        {
            //It's recommended to do a wait before retrying to open a port 
            if (Properties.arduino_logger.Default.LastGoodComPort.Length > 0)
            {
                if (TryOpenUsb(Properties.arduino_logger.Default.LastGoodComPort))
                    return;
                Thread.Sleep(2000);
            }

            if (Properties.arduino_logger.Default.ComPort.Length > 0)
            {
                if (TryOpenUsb(Properties.arduino_logger.Default.ComPort))
                    return;
                Thread.Sleep(2000);
            }

            foreach (var port in SerialPort.GetPortNames())
            {
                if (port != Properties.arduino_logger.Default.LastGoodComPort
                    && port != Properties.arduino_logger.Default.ComPort)
                {
                    if (true == TryOpenUsb(port))
                    {
                        Properties.arduino_logger.Default["LastGoodComPort"] = port;
                        Properties.arduino_logger.Default.Save();
                        lastGoodSerialPort = port;

                        return;
                    }
                    Thread.Sleep(2000);
                }
            }
        }

        private bool TryOpenUsb(string portName)
        {
            try
            {
                OpenUSB(portName);
                if ( true == TestPort())
                {
                    return true;
                }
                port.Close();
                Thread.Sleep(2000);
                return false;
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        //Try to read from serial port
        //Result:   True = got one byte
        //          False = Read timed out 
        private bool TestPort()
        {
            var testData = new char[10];
            try
            {
                Console.WriteLine("Testing Comport {0}", port.PortName);
                Console.WriteLine("Open = {0}", port.IsOpen);
                Console.WriteLine("CDHolding = {0}", port.CDHolding);
                Console.WriteLine("CtsHolding = {0}", port.CtsHolding);
                Console.WriteLine("DsrHolding = {0}", port.DsrHolding);
                Console.WriteLine("RtsEnable = {0}", port.RtsEnable);
                Console.WriteLine("DtrEnable = {0}", port.DtrEnable);
                Console.WriteLine("ReadTimeout = {0}", port.ReadTimeout);
                Console.WriteLine("Handshake = {0}", port.Handshake);
                Console.WriteLine("ReceivedBytesThreshold = {0}", port.ReceivedBytesThreshold);
                Console.WriteLine("BytesToRead = {0}", port.BytesToRead);
                port.Read(testData, 0, 1);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private void OpenUSB(string portName)
        {
            port = new SerialPort(portName);
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.ReadTimeout = 120000;
            port.WriteTimeout = 120000;
            port.StopBits = StopBits.One;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.NewLine = "\n";
            port.Open();
            Console.WriteLine("Port status: {0}", port.IsOpen);

            headpos = 0;
            tailpos = 0;
        }

        public string ReadString()
        {
            string result;
            if (port.BytesToRead == 0)
                return "";

            if (tailpos == max_tail)
            {
                byte[] copybuf = new byte[tailpos - headpos];
                Array.Copy(inputbuffer, headpos, copybuf, 0, tailpos - headpos);
                inputbuffer.Initialize();
                copybuf.CopyTo(inputbuffer, 0);
                headpos = 0;
                tailpos = copybuf.Length;
            }

            var x = port.BaseStream.Read(inputbuffer, tailpos, max_tail - tailpos);
            tailpos += x;

            for (int ix = headpos; ix < tailpos; ++ix)
            {
                if (inputbuffer[ix] == '\n')
                {
                    result = System.Text.Encoding.ASCII.GetString(inputbuffer, headpos, ix - headpos - 1); // not CR
                    headpos = ix + 1;   //cr + lf
                    return result;
                }
            }

            return "";
        }
    }
}
