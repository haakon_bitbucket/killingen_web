﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using KlimaApi.Models;

namespace KlimaApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TemperatursController : ApiController
    {
        private WebSiteEntities db = new WebSiteEntities();

        // GET: api/Temperaturs
        public IQueryable<Vanntemp> GetTemperaturs()
        {
            var res1 = db.Temperaturs.Where(t => t.SensorID == 1).Take(100).OrderByDescending(t => t.Tid)
                .Select(t =>
                  new Vanntemp
                  {
                      ID = t.ID,
                      SensorID = t.SensorID,
                      Tid = t.Tid,
                      Verdi = t.Verdi
                  });

            var res2 = db.Database.SqlQuery<Vanntemp>(@"
--select min(ROUND(Verdi,2)) AS [min], max(ROUND(Verdi, 2)) AS [Max], ROUND(AVG(Verdi), 2) as [Avg], CAST(Tid AS DATE) AS dato  
select max(ID) as [ID], max(SensorID) as [SensorID], max(ROUND(Verdi, 2)) AS [Verdi], CAST(Tid AS DATE) AS [Tid] from Environment.Temperatur
where sensorID = 1
group by CAST(Tid AS DATE)
order by CAST(Tid AS DATE) desc
            ");

            var res5 = db.GetVannTemperatur();
            var res4 = db.Temperaturs.Where(t => t.SensorID == 1).OrderByDescending(t => t.Tid).Select(t =>
                 new Vanntemp
                 {
                     ID = t.ID,
                     SensorID = t.SensorID,
                     Tid = t.Tid,
                     Verdi = t.Verdi
                 });

            //var r3 = (IQueryable<Vanntemp>)r2;
            //          r2.Select(t =>
            //new Vanntemp
            //{
            //    ID = t.ID,
            //    SensorID = 1,
            //    Tid = t.Tid,
            //    Verdi = t.Verdi
            //}) as IQueryable<Vanntemp>;


            List<Vanntemp> tempList = new List<Vanntemp>();
            tempList = res2.ToList<Vanntemp>();

            return tempList.AsQueryable();
        }

        // GET: api/Temperaturs/5
        [ResponseType(typeof(Vanntemp))]
        public IHttpActionResult GetTemperatur(int id)
        {

            Vanntemp temperatur = db.Temperaturs.Where(t => t.ID == id)
            .Select(t =>
            new Vanntemp
            {
                ID = t.ID,
                SensorID = t.SensorID,
                Tid = t.Tid,
                Verdi = t.Verdi
            }).Single();

            if (temperatur == null)
            {
                return NotFound();
            }

            return Ok(temperatur);
        }

        // PUT: api/Temperaturs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTemperatur(int id, Vanntemp temperatur)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != temperatur.ID)
            {
                return BadRequest();
            }

            db.Entry(temperatur).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TemperaturExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Temperaturs
        [ResponseType(typeof(Vanntemp))]
        public IHttpActionResult PostTemperatur(Vanntemp temperatur)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //db.Temperaturs.Add(temperatur);
            //db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = temperatur.ID }, temperatur);
        }

        // DELETE: api/Temperaturs/5
        [ResponseType(typeof(Temperatur))]
        public IHttpActionResult DeleteTemperatur(int id)
        {
            Temperatur temperatur = db.Temperaturs.Find(id);
            //if (temperatur == null)
            //{
            //    return NotFound();
            //}

            //db.Temperaturs.Remove(temperatur);
            //db.SaveChanges();

            return Ok(temperatur);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TemperaturExists(int id)
        {
            return db.Temperaturs.Count(e => e.ID == id) > 0;
        }
    }
}