﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KlimaApi.Models
{
    public class Vanntemp
    {
        public int ID { get; set; }
        public int SensorID { get; set; }
        public DateTime Tid { get; set; }
        public double Verdi { get; set; }

    }
}