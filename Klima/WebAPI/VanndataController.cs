﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Klima.WebAPI
{
    public class temps
    {
        public DateTime Tid { get; set; }
        public decimal Verdi { get; set; }
    }
    [Route("api/[controller]")]
    public class VanndataController : ApiController
    {
        private EnvironmentEntities db = new EnvironmentEntities();
        // GET api/<controller>
        public IEnumerable<temps> Get()
        {
            var last_temps = db.Database.SqlQuery<temps>(@"
    select max(t.Tid) as Tid, max(CAST(t.Verdi AS DECIMAL(4, 2))) as Verdi from Environment.Temperatur t
    where t.SensorID = 1
    GROUP BY((60 / 60) * DATEPART(hour, t.Tid) + FLOOR(DATEPART(minute, t.Tid) / 60))
    order by max(t.Tid) desc");
            return last_temps;
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}