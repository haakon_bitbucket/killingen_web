﻿//testing
//var para = document.createElement("p");
//var node = document.createTextNode("This is new 3.");
//para.appendChild(node);
//var element = document.getElementById("div1");
//element.appendChild(para);


// Set the dimensions of the canvas / graph
var margin = { top: 30, right: 20, bottom: 30, left: 50 },
    width = 600 - margin.left - margin.right,
    height = 270 - margin.top - margin.bottom;

// Parse the date / time
var parseDate = d3.time.format("%d-%b-%y").parse;

// Set the ranges
var x = d3.time.scale().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

// Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

// Define the line
var valueline = d3.svg.line()
    .x(function (d) { return x(d.dateTime); })
    .y(function (d) { return y(d.Verdi); });

// Adds the svg canvas
var svg = d3.select("body")
    .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
    .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

//data er serverdata, function() kalles async når data er tilgjengelig
d3.json("http://localhost:53022/Api/Temperaturs", function (error, data) {
    if (error) return console.warn(error);
    data.forEach(function (d) {
        //2016-05-27T00:00:00
        d.dateTime = d3.time.format("%Y-%m-%dT%H:%M:%S").parse(d.Tid);
        //2016-05-26T09:38:23.227
        //d.dateTime = d3.time.format("%Y-%m-%dT%H:%M:%S").parse(d.Tid.substr(0, 19));
        d.Verdi = +d.Verdi;
    });

//d3.csv("data.csv", function (error, data) {
//    data.forEach(function (d) {
//        d.date = parseDate(d.date);
//        d.close = +d.close;
//     });

    // Scale the range of the data
    x.domain(d3.extent(data, function (d) { return d.dateTime; }));
    y.domain([0, d3.max(data, function (d) { return d.Verdi; })]);

    // Add the valueline path.
    svg.append("path")
        .attr("class", "line")
        .attr("d", valueline(data));

    // Add the X Axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    // Add the Y Axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis);

});
