﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Klima;

namespace Klima.Controllers
{
    public class temps
    {
        public int Maalepunkt { get; set; }
        public DateTime Tid { get; set; }
        public decimal Verdi {get; set; }
    }
    public class BadevannController : Controller
    {
        private EnvironmentEntities db = new EnvironmentEntities();

        // GET: Badevann
        public ActionResult Index()
        {

            //            var last_temps = db.Database.SqlQuery<temps>(@"
            //    select max(t.Tid) as Tid, max(CAST(t.Verdi AS DECIMAL(4, 2))) as Verdi from Environment.Temperatur t
            //    where t.SensorID = 4
            //    GROUP BY((60 / 60) * DATEPART(hour, t.Tid) + FLOOR(DATEPART(minute, t.Tid) / 60))
            //    order by max(t.Tid) desc");
            //            //var temperatur = db.Temperatur.Include(t => t.Sensor);
            //            //var temperatur = db.GetVannTemperatur();

            //            var all_temps = db.Database.SqlQuery<temps>(@"
            //    select Tid, CAST(t.Verdi AS DECIMAL(4, 2)) as Verdi from Environment.Temperatur t
            //    where t.SensorID = 4
            //    order by t.Tid desc");

            var all_temps = db.Database.SqlQuery<temps>(@"
                select Maalepunkt = m.ID
	                            , Tid = cast(CAST(t.Tid as date) as datetime)
	                            , Verdi=Cast(MAX(t.Verdi) AS DECIMAL(4,2))
	                            from Environment.Temperatur t
                            join Environment.Sensor s on t.SensorID = s.ID
                            join Environment.Maalepunkt m on m.ID = s.MaalepunktID 
                            WHERE SensorID in (
	                            select s2.ID from WebSite.Environment.Sensor s2
	                            where s2.MaalepunktID = 2 
	                            or s2.MaalepunktID = 3
                            )
                            group by cast(CAST(t.Tid as date) as datetime)
	                            , m.ID
	                            , t.SensorId
                union all
                select Maalepunkt = m.ID
	                            , Tid = cast(CAST(t.Tid as date) as datetime)
	                            , Verdi=Cast(MIN(t.Verdi) AS DECIMAL(4,2))
	                            from Environment.Temperatur t
                            join Environment.Sensor s on t.SensorID = s.ID
                            join Environment.Maalepunkt m on m.ID = s.MaalepunktID 
                            WHERE SensorID in (
	                            select s2.ID from WebSite.Environment.Sensor s2
	                            where s2.MaalepunktID = 2 
	                            or s2.MaalepunktID = 3
                            )
                            group by cast(CAST(t.Tid as date) as datetime)
	                            , m.ID
	                            , t.SensorId
                UNION ALL 
                select Maalepunkt = m.ID
	                            , Tid = dateadd(HH,datepart(HOUR,T.Tid), cast(CAST(t.Tid as date) as datetime))
	                            , Verdi=Cast(MIN(t.Verdi) AS DECIMAL(4,2))
	                            from Environment.Temperatur t
                            join Environment.Sensor s on t.SensorID = s.ID
                            join Environment.Maalepunkt m on m.ID = s.MaalepunktID 
                            WHERE SensorID in (
	                            select s2.ID from WebSite.Environment.Sensor s2
	                            where s2.MaalepunktID = 2 
	                            or s2.MaalepunktID = 3
                            )
			                and t.Tid > dateadd(DD, -7, CAST(GETDATE() as datetime))
                            group by dateadd(HH,datepart(HOUR,t.Tid), cast(CAST(t.Tid as date) as datetime))
	                            , m.ID
	                            , t.SensorId
                            order by Tid desc
                ");

            //var all_temps = db.Database.SqlQuery<temps>(@"
            //    select SensorId
            //        , Tid = dateadd(DD, datepart(DD, Environment.Temperatur.Tid), cast(CAST(Environment.Temperatur.Tid as date) as datetime))
            //        , Verdi = Cast(MAX(Environment.Temperatur.Verdi) AS DECIMAL(4, 2))
            //    from Environment.Temperatur
            //    WHERE SensorID in (
            //        select s.ID from WebSite.Environment.Sensor s
            //        where s.MaalepunktID = 2
            //        or s.MaalepunktID = 3
            //    )
            //    group by dateadd(DD, datepart(DD, Environment.Temperatur.Tid), cast(CAST(Environment.Temperatur.Tid as date) as datetime))
            //     , SensorId
            //    order by Tid desc
            //");

            return View(all_temps.ToList<temps>());
        }

        // GET: Badevann/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            return View(temperatur);
        }

        // GET: Badevann/Create
        public ActionResult Create()
        {
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn");
            return View();
        }

        // POST: Badevann/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SensorID,Tid,Verdi")] Temperatur temperatur)
        {
            if (ModelState.IsValid)
            {
                db.Temperatur.Add(temperatur);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // GET: Badevann/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // POST: Badevann/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SensorID,Tid,Verdi")] Temperatur temperatur)
        {
            if (ModelState.IsValid)
            {
                db.Entry(temperatur).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // GET: Badevann/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            return View(temperatur);
        }

        // POST: Badevann/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Temperatur temperatur = db.Temperatur.Find(id);
            db.Temperatur.Remove(temperatur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
