﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Klima;

namespace Klima.Controllers
{
    public class TemperatursController : Controller
    {
        private EnvironmentEntities db = new EnvironmentEntities();

        // GET: Temperaturs
        public ActionResult Index()
        {
            var temperatur = db.Temperatur.Include(t => t.Sensor);
            return View(temperatur.ToList());
        }

        // GET: Temperaturs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            return View(temperatur);
        }

        // GET: Temperaturs/Create
        public ActionResult Create()
        {
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn");
            return View();
        }

        // POST: Temperaturs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SensorID,Tid,Verdi")] Temperatur temperatur)
        {
            if (ModelState.IsValid)
            {
                db.Temperatur.Add(temperatur);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // GET: Temperaturs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // POST: Temperaturs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SensorID,Tid,Verdi")] Temperatur temperatur)
        {
            if (ModelState.IsValid)
            {
                db.Entry(temperatur).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SensorID = new SelectList(db.Sensors, "ID", "Navn", temperatur.SensorID);
            return View(temperatur);
        }

        // GET: Temperaturs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Temperatur temperatur = db.Temperatur.Find(id);
            if (temperatur == null)
            {
                return HttpNotFound();
            }
            return View(temperatur);
        }

        // POST: Temperaturs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Temperatur temperatur = db.Temperatur.Find(id);
            db.Temperatur.Remove(temperatur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
