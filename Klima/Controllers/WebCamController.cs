﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Klima;

namespace Klima.Controllers
{
    public class WebCamController : Controller
    {
        private EnvironmentEntities db = new EnvironmentEntities();

        // GET: WebCam
        public ActionResult Index()
        {
            var temperatur = db.Temperatur.Include(t => t.Sensor);
            return View(temperatur.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult OppdaterBilde()
        {
            ImageData img = new ImageData(GetPicturePath(), "Nordskjæret");
            return PartialView("UpdatePicture", model: img);
        }

        private string GetPicturePath()
        {
            return "/snapshots/snapshot.jpg";
        }
    }

    public class ImageData
    {
        public ImageData() { }
        public ImageData(string p, string a)
        {
            Path = p;
            Alternate = a;
        }

        public string Path { get; set; }
        public string Alternate { get; set; }
    }
}
