﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace Klima.Controllers
{
    public class temperaturer
    {
        public double now;
        public DateTime nowTime;
        public double max;
        public DateTime maxTime;
        public double min;
        public DateTime minTime;
    }

    public class HomeController : Controller
    {
        private EnvironmentEntities db = new EnvironmentEntities();

        public ActionResult Index()
        {
            //var temperatur = db.Temperatur.Include(t => t.Sensor);
            Temperatur current_temperatur;
            Temperatur max_temperatur;
            Temperatur min_temperatur;
            var måling = new temperaturer();
            try
            {
                current_temperatur = db.Temperatur.Where(t => t.Sensor.MaalepunktID == 3).OrderByDescending(t => t.Tid).First();
                max_temperatur = db.Temperatur.Where(t => t.Sensor.MaalepunktID == 3 && t.Tid >= DateTime.Today).OrderByDescending(t => t.Verdi).First();
                min_temperatur = db.Temperatur.Where(t => t.Sensor.MaalepunktID == 3 && t.Tid >= DateTime.Today).OrderBy(t => t.Verdi).First();
                måling.now = current_temperatur.Verdi;
                måling.nowTime = current_temperatur.Tid;
                måling.max = max_temperatur.Verdi;
                måling.maxTime = max_temperatur.Tid;
                måling.min = min_temperatur.Verdi;
                måling.minTime = min_temperatur.Tid;
            }
            catch ( Exception ex)
            { }

            return View(måling);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}