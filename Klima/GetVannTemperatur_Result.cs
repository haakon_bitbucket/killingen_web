//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Klima
{
    using System;
    
    public partial class GetVannTemperatur_Result
    {
        public Nullable<System.DateTime> Tid { get; set; }
        public Nullable<decimal> Verdi { get; set; }
    }
}
