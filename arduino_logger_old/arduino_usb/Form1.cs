﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

using arduino_usb;

namespace arduino_usb
{
    public partial class Form1 : Form
    {
        websiteEntities context = new websiteEntities();
        Random rdn = new Random();
        string sensor1;
        string sensor2;
        string sensor3;
        string sensor4;

        public Form1()
        {
            InitializeComponent();

            var sensors = context.sensors;

            sensor1 = "28B27DDC06000034";
            sensor2 = "286D4DDD060000DF";
            sensor3 = "28902EDD060000B8";
            sensor4 = "28EF7EDD06000091";

            var usb = new arduino_usb.ReadUSB();
            usb.OpenUSB();

            //var graf = new Graf();
            //graf.Show();

            SetUpChart();

            updatechart(usb);
        }

        public async void updatechart(arduino_usb.ReadUSB usb)
        {
            while(true)
            {
                //var tempdata = usb.ReadStringPort(port);
                //Console.Write(tempdata);
                //var pt1 = await usb.ReadPortPoint(false);

                string dat = await usb.ReadString();
                Console.Write(dat);

                if (dat == "")
                {
                    Console.WriteLine("None");
                    continue;
                }
                //if (dat.Length < 24)
                //    continue;

                string id;
                float f;

                try
                {
                    var item = dat.Split(';');
                    id = item[0];

                    string tempf = item[1]; ;
                    tempf = tempf.Replace(".", ",");
                    f = float.Parse(tempf);

                    if (id == sensor1)
                        AddDatapoint("Series1", id, DateTime.Now, f);
                    else if (id == sensor2)
                        AddDatapoint("Series2", id, DateTime.Now, f);
                    else if (id == sensor3)
                        AddDatapoint("Series3", id, DateTime.Now, f);
                    else if (id == sensor4)
                        AddDatapoint("Series4", id, DateTime.Now, f);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

       public void SetUpChart()
        {
            chart1.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series["Series1"].Color = Color.Red;
            chart1.Series["Series1"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;

            chart1.Series["Series2"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series["Series2"].Color = Color.Blue;
            chart1.Series["Series2"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;

            chart1.Series["Series3"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series["Series3"].Color = Color.Green;
            chart1.Series["Series3"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;

            chart1.Series["Series4"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series["Series4"].Color = Color.Orange;
            chart1.Series["Series4"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;

            chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "G";
        }

        public Point GetTestPoint()
        {
            Point pt = new Point(rdn.Next(0, 10), rdn.Next(0, 10));
            return pt;
        }

        public void AddDatapoint(string serie, string sensorNavn, DateTime x, float y)
        {
            if (y < -126.0)
                return;
            try
            {
                var sensorId = (from s in context.sensors where s.navn == sensorNavn select s.ID).First();
                temperatur temp = new temperatur { sensorID = sensorId, tid = x, verdi = y};
                temp.sensorID = sensorId;
                temp.verdi = y;
                temp.tid = x;
                context.temperaturs.Add(temp);
                context.SaveChanges();
                chart1.Series[serie].Points.AddXY(x, y);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Plot(ref System.Windows.Forms.DataVisualization.Charting.Series serie, int x, int y)
        {
            serie.Points.AddXY(x, y);
        }
    }
}
