﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace arduino_usb
{
    public partial class Graf : Form

    {
        Random rdn = new Random();
        public Graf()
        {
            InitializeComponent();
            SetUpChart();
        }

        public void SetUpChart()
        {
            TempGraf.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            TempGraf.Series["Series1"].Color = Color.Red;

            TempGraf.Series["Series2"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            TempGraf.Series["Series2"].Color = Color.Blue;

            TempGraf.Series["Series3"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            TempGraf.Series["Series3"].Color = Color.Blue;

            TempGraf.Series["Series4"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            TempGraf.Series["Series4"].Color = Color.Blue;
        }

        public Point GetTestPoint()
        {
            Point pt = new Point(rdn.Next(0, 10), rdn.Next(0, 10));
            return pt;
        }

        public void AddDatapoint(string serie, int x, int y)
        {
            TempGraf.Series[serie].Points.AddXY(x, y);
        }

        public void Plot(ref System.Windows.Forms.DataVisualization.Charting.Series serie, int x, int y)
        {
            serie.Points.AddXY(x, y);
        }
    }
}
