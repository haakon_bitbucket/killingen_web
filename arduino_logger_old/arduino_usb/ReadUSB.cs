﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.IO.Ports;
using System.Configuration;

namespace arduino_usb
{
    public class ReadUSB
    {
        Random rdn = new Random();
        public SerialPort port;
        bool test = true;
        const int max_tail = 50;
        byte[] inputbuffer = new byte[max_tail];
        int headpos;
        int tailpos;

        public SerialPort OpenUSB()
        {
            var comPort = Properties.Settings.Default["ComPort"].ToString();
            port = new SerialPort("COM4");

            try
            {
                port.BaudRate = 9600;
                port.Parity = Parity.None;
                port.ReadTimeout = 10;
                port.StopBits = StopBits.One;
                port.DtrEnable = true;
                port.RtsEnable = true;
                port.NewLine = "\n";
                port.Open();

                headpos = 0;
                tailpos = 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return port;
        }

        public string ReadStringPort(SerialPort port, bool test)
        {
            string buffer;
            //port.Write("Hello World!");
            //Console.WriteLine("Wrote to the port " + port);

            if (test == true)
            {
                return "";
            }

            buffer = port.ReadLine();
            return buffer;
        }

        public byte[] ReadPort(SerialPort port)
        {
            byte[] buffer = new byte[512];
            //port.Write("Hello World!");
            //Console.WriteLine("Wrote to the port " + port);
            Thread.Sleep(1000);
            port.Read(buffer, 0, (int)buffer.Length);
            return buffer;
        }

        public async Task<Tuple<DateTime, int>> ReadPortPoint(bool test)
        {
            byte[] buffer = new byte[512];
            //port.Write("Hello World!");
            //Console.WriteLine("Wrote to the port " + port);

            if (test == true)
            {
                await Task.Delay(1000);
                return Tuple.Create(DateTime.Now, rdn.Next());
            }

            var x = port.BaseStream.Read(buffer, 0, 512);

            string result = System.Text.Encoding.UTF8.GetString(buffer);

            return Tuple.Create(DateTime.Now, rdn.Next());
        }

        public async Task<string> ReadPortPoint2(bool test)
        {
            byte[] buffer = new byte[64];

            var x = await port.BaseStream.ReadAsync(buffer, 0, 24);
            Console.WriteLine(System.Text.Encoding.UTF8.GetString(buffer, 0, x));

            if (x != 24)
            {
                while (x != 0 && x != 24)
                {
                    x = await port.BaseStream.ReadAsync(buffer, 0, 24);
                    Console.WriteLine(System.Text.Encoding.UTF8.GetString(buffer, 0, x));
                }
                if (x != 24)
                    return "";
            }

            string result = System.Text.Encoding.UTF8.GetString(buffer, 0, x);

            return result;
        }

        public async Task<string> ReadString()
        {
            string result;

            try
            {
                if (tailpos == max_tail)
                {
                    byte[] copybuf = new byte[tailpos - headpos];
                    Array.Copy(inputbuffer, headpos, copybuf, 0, tailpos - headpos);
                    inputbuffer.Initialize();
                    copybuf.CopyTo(inputbuffer, 0);
                    headpos = 0;
                    tailpos = copybuf.Length;
                }

                var x = await port.BaseStream.ReadAsync(inputbuffer, tailpos, max_tail - tailpos);
                tailpos += x;


                for (int ix = headpos; ix < tailpos; ++ix)
                {
                    if (inputbuffer[ix] == '\n')
                    {
                        result = System.Text.Encoding.ASCII.GetString(inputbuffer, headpos, ix - headpos - 1); // not CR
                        headpos = ix + 1;   //cr + lf
                        return result;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return "";
        }
    }
}
