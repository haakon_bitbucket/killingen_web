﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace arduino_logger
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new LoggerService()
            };
            bool logToConsole = Properties.arduino_logger.Default.LogToConsole;
            if (logToConsole == true)
            {
                LoggerService logger = ServicesToRun[0] as LoggerService;
                logger.DoLogging(null, null);
            }
            else
            {
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
