﻿using System;
using System.IO.Ports;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;

namespace arduino_logger
{
    public class ReadUSB :IDisposable
    {
        public SerialPort port;
        const int max_tail = 50;
        byte[] inputbuffer = new byte[max_tail];
        int headpos;
        int tailpos;
        bool logToConsole { get; set; }
        string lastGoodSerialPort = "";

        public ReadUSB()
        {
            logToConsole = Properties.arduino_logger.Default.LogToConsole;
            OpenUSBSerial();
        }

        public void Dispose()
        {
            port.Close();
        }

        private void OpenUSBSerial()
        {
            if (Properties.arduino_logger.Default.LastGoodComPort.Length > 0)
            {
                if (TryOpenUsb(Properties.arduino_logger.Default.LastGoodComPort))
                    return;
            }

            if (Properties.arduino_logger.Default.ComPort.Length > 0)
            {
                if (TryOpenUsb(Properties.arduino_logger.Default.ComPort))
                    return;
            }

            foreach (var port in SerialPort.GetPortNames())
            {
                if (port != Properties.arduino_logger.Default.LastGoodComPort
                    && port != Properties.arduino_logger.Default.ComPort)
                {
                    if (true == TryOpenUsb(port))
                    {
                        Properties.arduino_logger.Default["LastGoodComPort"] = port;
                        Properties.arduino_logger.Default.Save();
                        lastGoodSerialPort = port;

                        return;
                    }
                }
            }
        }

        private bool TryOpenUsb(string portName)
        {
            OpenUSB(portName);
            if ( true == TestPort())
            {
                return true;
            }
            try
            {
                port.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("TryOpenUSB exception: {0}", ex.Message);
            }
            //It's recommended to do a wait before retrying to open a port 
            Thread.Sleep(10000);
            return false;
        }

        //Try to read from serial port
        //Result:   True = got one byte
        //          False = Read timed out 
        private bool TestPort()
        {
            var testData = new char[10];
            try
            {
                Console.WriteLine("Testing Comport {0}", port.PortName);
                Console.WriteLine("Open = {0}", port.IsOpen);
                Console.WriteLine("CDHolding = {0}", port.CDHolding);
                Console.WriteLine("CtsHolding = {0}", port.CtsHolding);
                Console.WriteLine("DsrHolding = {0}", port.DsrHolding);
                Console.WriteLine("RtsEnable = {0}", port.RtsEnable);
                Console.WriteLine("DtrEnable = {0}", port.DtrEnable);
                Console.WriteLine("ReadTimeout = {0}", port.ReadTimeout);
                Console.WriteLine("Handshake = {0}", port.Handshake);
                Console.WriteLine("ReceivedBytesThreshold = {0}", port.ReceivedBytesThreshold);
                Console.WriteLine("BytesToRead = {0}", port.BytesToRead);
                port.Read(testData, 0, 1);
            }
            catch (Exception ex)
            {
                Console.WriteLine("TestPort exception: {0}", ex.Message);
                return false;
            }

            return true;
        }

        private void OpenUSB(string portName)
        {
            port = new SerialPort(portName);
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.ReadTimeout = 10000;
            port.WriteTimeout = 10000;
            port.StopBits = StopBits.One;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.NewLine = "\n";
            port.Open();
            Console.WriteLine("Port status: {0}", port.IsOpen);

            headpos = 0;
            tailpos = 0;
        }

        public string ReadString()
        {
            string result;

            if (tailpos == max_tail)
            {
                byte[] copybuf = new byte[tailpos - headpos];
                Array.Copy(inputbuffer, headpos, copybuf, 0, tailpos - headpos);
                inputbuffer.Initialize();
                copybuf.CopyTo(inputbuffer, 0);
                headpos = 0;
                tailpos = copybuf.Length;
            }

            var x = port.BaseStream.Read(inputbuffer, tailpos, max_tail - tailpos);
            tailpos += x;

            for (int ix = headpos; ix < tailpos; ++ix)
            {
                if (inputbuffer[ix] == '\n')
                {
                    result = System.Text.Encoding.ASCII.GetString(inputbuffer, headpos, ix - headpos - 1); // not CR
                    headpos = ix + 1;   //cr + lf
                    return result;
                }
            }

            return "";
        }

        //http://www.sparxeng.com/blog/software/must-use-net-system-io-ports-serialport
        public void AsyncRead()
        {
            const int blockLimit = 512;
            byte[] buffer = new byte[blockLimit];

            Action kickoffRead = null;

            kickoffRead = delegate
            {
                port.BaseStream.BeginRead(buffer, 0, buffer.Length, delegate (IAsyncResult ar)
                {
                    try
                    {
                        int actualLength = port.BaseStream.EndRead(ar);
                        byte[] received = new byte[actualLength];
                        Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
                        raiseAppSerialDataEvent(received);
                    }
                    catch (IOException exc)
                    {
                        //handleAppSerialError(exc);
                    }
                    kickoffRead();
                }, null);
            };
            kickoffRead();
        }

        //To decode the received data from the port.   
        private void raiseAppSerialDataEvent(byte[] Data)
        {
            string Result = Encoding.Default.GetString(Data);
            //TextBox1.Invoke((Action)delegate
            //{
            //    TextBox1.Text = Result; TextBox1.Refresh();
            //});
        }
    }
}
