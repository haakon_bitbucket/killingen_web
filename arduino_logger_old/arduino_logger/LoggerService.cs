﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DataContext;

namespace arduino_logger
{
    public partial class LoggerService : ServiceBase
    {
        public DataContext.TemperaturDataClassesDataContext db;
        //public string sensor1 = "28B27DDC06000034";
        //public string sensor2 = "286D4DDD060000DF";
        //public string sensor3 = "28902EDD060000B8";
        //public string sensor4 = "28EF7EDD06000091";
        public ReadUSB usb;
        bool logToConsole = false;
        bool logToDatabase = true;

        public LoggerService()
        {
            InitializeComponent();

            //eventLog = new System.Diagnostics.EventLog();
            //eventLog.Source = "ArduinoDataLogger";
            //if (!System.Diagnostics.EventLog.SourceExists("ArduinoDataLogger"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource("ArduinoDataLogger", "Application");
            //}

            //eventLog.WriteEntry("LoggerService() - 1", EventLogEntryType.Information);

            logToConsole = Properties.arduino_logger.Default.LogToConsole;
            logToDatabase = Properties.arduino_logger.Default.NoDatabase == false;

            eventLog.WriteEntry("LoggerService() - 2", EventLogEntryType.Information);
            Console.Beep(440, 500);
        }

        public void LoopDoLogging()
        {
            while (true)
            {
                DoLogging(null, null);
                Thread.Sleep(10000);
            }
        }        
        public void DoLogging(object sender, System.Timers.ElapsedEventArgs args)
        {
            //while (true)
            //{
                try
                {
                    using (db = new DataContext.TemperaturDataClassesDataContext())
                    {
                        using (usb = new ReadUSB())
                        {
                            while (true)
                            {
                                string dat = usb.ReadString();
                                AddDatapoint(dat);
                                Thread.Sleep(10000);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (logToConsole)
                    {
                        eventLog.WriteEntry("DoLogging exception (no more data?): " + ex.Message, EventLogEntryType.Error);
                        //Thread.Sleep(10000);
                    }
                }
            //}
        }

        public float ParseFloat(string input)
        {
            float f1 = 0;

            bool res1 = float.TryParse(input, out f1);
            if (res1 == false)
            {
                bool res2 = float.TryParse(input.Replace(".", System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator), out f1);
                if (res2 == false)
                {
                    f1 = float.Parse(input.Replace(",", System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator));
                }
            }
            return f1;
        }

        public void AddDatapoint(string dataPoint)
        {
            if (dataPoint.Length !=22)
            {
                return;
            }

            var item = dataPoint.Split(';');
            var sensorNavn = item[0];

            float y_val = ParseFloat(item[1]);

            if (y_val < -126.0)
                return;

            if (logToDatabase)
            {
                var sensorId = (from s in db.Sensors where s.Navn == sensorNavn select s.ID).First();
                var temp = new DataContext.Temperatur();
                temp.SensorID = sensorId;
                temp.Verdi = y_val;
                temp.Tid = DateTime.Now;
                db.Temperaturs.InsertOnSubmit(temp);
                db.SubmitChanges();
                if (logToConsole)
                    Console.WriteLine("Sensor {0}; {1} {2}", temp.SensorID, temp.Tid, temp.Verdi);
            }
        }

        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry("OnStart() - 1", EventLogEntryType.Information);
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 180000; // 60 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.DoLogging);
            timer.Start();
            eventLog.WriteEntry("OnStart() - 2", EventLogEntryType.Information);
            Console.Beep(440, 1000);
        }

        protected override void OnStop()
        {
        }
    }
}
