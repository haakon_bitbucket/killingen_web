﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VanndataAPI.Models;

namespace VanndataAPI.Repository
{
        public interface IVanndataRepository
    {
        void Add(Vanndata item);
        IEnumerable<Vanndata> GetAll();
        Vanndata Find(string key);
        void Remove(string Id);
        void Update(Vanndata item);
    }
}
