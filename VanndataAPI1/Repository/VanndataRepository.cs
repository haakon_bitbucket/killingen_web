﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VanndataAPI.Models;

namespace VanndataAPI.Repository
{
    public class VanndataRepository : IVanndataRepository
    {
        static List<Vanndata> VanndataList = new List<Vanndata>();

        public void Add(Vanndata item)
        {
            VanndataList.Add(item);
        }

        public Vanndata Find(string key)
        {
            return VanndataList
                .Where(e => e.Tid.Equals(key))
                .SingleOrDefault();
        }

        public IEnumerable<Vanndata> GetAll()
        {
            return VanndataList;
        }

        public void Remove(string Id)
        {
            var itemToRemove = VanndataList.SingleOrDefault(r => r.Tid.ToString() == Id);
            if (itemToRemove != null)
                VanndataList.Remove(itemToRemove);
        }

        public void Update(Vanndata item)
        {
            var itemToUpdate = VanndataList.SingleOrDefault(r => r.Tid == item.Tid);
            if (itemToUpdate != null)
            {
                itemToUpdate.Verdi = item.Verdi;
            }
        }
    }
}
