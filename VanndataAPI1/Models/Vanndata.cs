﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanndataAPI.Models
{
    public class Vanndata
    {
        public DateTime Tid { get; set; }
        public decimal Verdi{ get; set; }
    }
}
