﻿using System;
using System.Collections.Generic;
using VanndataAPI.Models;
using VanndataAPI.Repository;
using Microsoft.AspNet.Mvc;

namespace VanndataAPI.Controllers
{
    [Route("api/[controller]")]
    public class VanndataController : Controller
    {
        [FromServices]
        public IVanndataRepository VanndataRepo { get; set; }

        [HttpGet]
        public IEnumerable<Vanndata> GetAll()
        {
            return VanndataRepo.GetAll();
        }

        [HttpGet("{id}", Name = "GetVanndata")]
        public IActionResult GetById(string id)
        {
            var item = VanndataRepo.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Vanndata item)
        {
            if (item == null)
            {
                return HttpBadRequest();
            }
            VanndataRepo.Add(item);
            return CreatedAtRoute("GetVanndata", new { Controller = "Vanndata", id = item.Tid }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] Vanndata item)
        {
            if (item == null)
            {
                return HttpBadRequest();
            }
            var contactObj = VanndataRepo.Find(id);
            if (contactObj == null)
            {
                return HttpNotFound();
            }
            VanndataRepo.Update(item);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            VanndataRepo.Remove(id);
        }
    }
}
