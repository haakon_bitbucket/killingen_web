﻿using System;
using System.Collections.Generic;
using System.Linq;
using VanndataApi.Models;

namespace VanndataApi.Repository
{
    public interface IVanndataRepository
    {
        void Add(Vanndata item);
        IEnumerable<Vanndata> GetAll();
        Vanndata Find(string key);
        void Remove(string Id);
        void Update(Vanndata item);
    }
}
