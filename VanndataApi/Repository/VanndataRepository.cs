﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VanndataApi.Models;

namespace VanndataApi.Repository
{
    public class VanndataRepository : IVanndataRepository
    {
        static List<Vanndata> VanndataList = new List<Vanndata>();

        public void Add(Vanndata item)
        {
            VanndataList.Add(item);
        }

        public Vanndata Find(string key)
        {
            return VanndataList
                .Where(e => e.ID.Equals(int.Parse(key)))
                .SingleOrDefault();
        }

        public IEnumerable<Vanndata> GetAll()
        {
            return VanndataList;
        }

        public void Remove(string Id)
        {
            var itemToRemove = VanndataList.SingleOrDefault(r => r.ID == int.Parse(Id));
            if (itemToRemove != null)
                VanndataList.Remove(itemToRemove);
        }

        public void Update(Vanndata item)
        {
            var itemToUpdate = VanndataList.SingleOrDefault(r => r.ID == item.ID);
            if (itemToUpdate != null)
            {
            itemToUpdate.Tid = item.Tid;
            itemToUpdate.Verdi = item.Verdi;
            }
        }
    }
}
