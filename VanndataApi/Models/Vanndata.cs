﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanndataApi.Models
{
    public class Vanndata
    {
        public int ID { get; set; }
        public DateTime Tid { get; set; }
        public decimal Verdi { get; set; } 
    }
}
