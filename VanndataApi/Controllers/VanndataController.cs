﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using VanndataApi.Models;
using VanndataApi.Repository;

namespace VanndataApi.Controllers
{
    [Route("api/[controller]")]
    public class VanndataController : Controller
    {
        [FromServices]
        public IVanndataRepository VanndataRepo { get; set; }

        [HttpGet]
        public IEnumerable<Vanndata> GetAll()
        {
            return VanndataRepo.GetAll();
        }

        [HttpGet("{id}", Name = "GetVanndata")]
        public IActionResult GetById(string id)
        {
            var item = VanndataRepo.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Vanndata item)
        {
            if (item == null)
            {
                return HttpBadRequest();
            }
            VanndataRepo.Add(item);
            return CreatedAtRoute("GetVanndata", new { Controller = "Vanndata", id = item.Tid }, item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] Vanndata item)
        {
            if (item == null)
            {
                return HttpBadRequest();
            }
            var contactObj = VanndataRepo.Find(id);
            if (contactObj == null)
            {
                return HttpNotFound();
            }
            VanndataRepo.Update(item);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            VanndataRepo.Remove(id);
        }
    }
}

//default
//// GET: api/values
//[HttpGet]
//public IEnumerable<string> Get()
//{
//    return new string[] { "value1", "value2" };
//}

//// GET api/values/5
//[HttpGet("{id}")]
//public string Get(int id)
//{
//    return "value";
//}

//// POST api/values
//[HttpPost]
//public void Post([FromBody]string value)
//{
//}

//// PUT api/values/5
//[HttpPut("{id}")]
//public void Put(int id, [FromBody]string value)
//{
//}

//// DELETE api/values/5
//[HttpDelete("{id}")]
//public void Delete(int id)
//{
//}

